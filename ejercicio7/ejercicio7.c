#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
	int grado,i;
	double *coeficientes;
	double variableX,coeficiente,acumulador, valor;
	printf("Ingrese el grado del polinomio \n");
	scanf("%d",&grado);
	coeficientes = (double*) malloc(grado*sizeof(double));
	for(i=0;i<grado;i++){
		printf("Ingrese coeficiente de x%d: \n", i);
		scanf("%lf",&coeficiente);
		coeficientes[i]=coeficiente;
	}

	printf("Ingrese el valor de la variable X\n");
	scanf("%lf",&variableX);
	for(i=0;i<grado;i++){
		valor=coeficientes[i];
		acumulador=acumulador+((valor)*(pow(variableX,i)));
	}

	printf("resultado de evaluar X=%f en P(X): %f siendo: ",variableX,acumulador);
	printf("\n");
	printf("P(X)= ");
	for(int contador=0;contador<grado;contador++){
		if(coeficientes[contador]!=0){
			if(contador==0){
				printf("%.2f",coeficientes[contador]);
			}else{
				if(coeficientes[contador]<0){
					printf(" %.2fX%d",coeficientes[contador],contador);
				}else{
					printf("+ %.2fX%d",coeficientes[contador],contador);
				}
			}
		}
	}
printf("\n");
return 0;
}


